%just run this script, at the end it will display how many it got right


load('apples.mat');
load('apples2.mat');
load('oranges.mat');
load('oranges2.mat');
appledata = [apples apples2];
orangedata = [oranges oranges2];
result = 0;
parfor i = 1:length(apples)
    if AO_discriminate(apples(:,i))==1
result= result+1;
    end
end
parfor i = 1:length(apples2)
    if AO_discriminate(apples2(:,i))==1
result= result+1;
    end
end
parfor i = 1:length(oranges)
    if AO_discriminate(oranges(:,i))==0
result= result+1;
    end
end
parfor i = 1:length(oranges2)
    if AO_discriminate(oranges2(:,i))==0
result= result+1;
    end
end

disp([num2str(result) ' out of ' num2str(length(apples)+length(apples2)+length(oranges)+length(oranges2))])




