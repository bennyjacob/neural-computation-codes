function [result,result2,result3,result4,result5] = AO_discriminate(x)
results = multi_layers();
w = results{1};
w0 = results{2};
v = results{3};
v0 = results{4};
result2 = results{5};
result4 = results{6};
result5 = results{7};
datay=results{8};
y2=result4;
y1=result5;
blah = @(x_x) (y2(2)-y2(1)/(y1(2)-y1(1)))*(x_x-y1(1))+y2(1);
sigmoid = @(xx) 1 ./ (1 + exp(-xx));
y = sigmoid(w'*x+w0 * ones(1,size(x,2)));
z = sigmoid(v' * y + v0);
hpoint = blah(y(1));
rpoint = y(2);
% if rpoint>hpoint
%     result = 0;
% else
%     result = 1;
% end
if z>0.5
    result = 1;
else
    result = 0;
end

result3 = z;

end

