function [zz,w,w0,v,v0] = partB(data,hidden,teacher)
%hidden = 4;  % number of hidden units

sigmoid = @(x) 1 ./ (1 + exp(-x));

% initialize data array
% data = [
%         1 1 1 0 1 0 0 1 0;
%         0 0 1 1 1 1 0 0 1;
%         0 1 0 0 1 0 1 1 1;
%         1 0 0 1 1 1 1 0 0;
%         0 1 1 0 1 0 1 1 0;
%         1 0 0 1 1 1 0 0 1;
%         0 1 1 0 1 0 1 1 0;
%         1 0 0 1 1 1 0 0 1;
%         ]';
[N K] = size(data);

% initialize teacher
%teacher = [ ones(1, K/2) zeros(1, K/2) ];
half1   = 1 : K/2;
half2   = K/2 + half1;

% learning rate
eta   = .1;
alpha = .2;  % momentum term

% number of trials
num_trials = 10000;
E      = zeros(1, num_trials);
missed = zeros(1, num_trials);

% initialize weights
v  = randn(hidden,1);
v0 = randn(1);
w  = randn(N,hidden);
w0 = randn(hidden,1);
dw = 0; dw0 = 0; dv = 0; dv0 = 0;

% loop over trials
for t = 1 : num_trials
   
    % compute y layer
    y = sigmoid(w' * data + w0 * ones(1, K));
    
    % compute z layer
    z = sigmoid(v' * y + v0);
    
    % compute error
    delta     = teacher - z;
    E(t)      = delta * delta';
    missed(t) = sum( abs( (z > 0.5) - teacher) );
    
    % compute delta_z
    dsig    = z .* (1 - z);
    delta_z = delta .* dsig;
    
    % compute delta_y
    dsig    = y .* (1 - y);
    delta_y = dsig .* (v * delta_z);
    
    % accumulate dw
    dw  = eta * data * delta_y' + alpha * dw;
    dw0 = eta * sum(delta_y, 2) + alpha * dw0;
    
    % accumulate dv
    dv  = eta * y * delta_z' + alpha * dv ;
    dv0 = eta * sum(delta_z) + alpha * dv0;
    
    % update weights
    w  = w  + dw;
    w0 = w0 + dw0;
    v  = v  + dv;
    v0 = v0 + dv0;
    
end

% plot
% figure(1);
% plot(E);
% title('Error');
% 
% figure(2);
% plot(missed);
% title('Letters misclassified');
% 
% figure(3);
% for i = 1 : hidden
%     subplot(1, hidden, i);
%     imagesc( reshape(w(:, i), 3, 3) );
%     colormap gray; axis square;
%     set(gca,'ytick',[]); set(gca,'xtick',[]);
%     title(sprintf('$w_%d$',i));
% end
% 
% % print
% set(0,'defaulttextinterpreter','none');
% %printfig(1,sprintf('q4error_%d',hidden));
% %printfig(2,sprintf('q4missed_%d',hidden));
% %printfig(3,sprintf('q4w_%d',hidden));
zz = [];
for i = 1:length(z)
    if z(i)>0.5
        zz(i) = 1;
    else
        zz(i) = 0;
    end
end

        
end