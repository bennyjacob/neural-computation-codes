function [a] = partB1(data,k,hidden)


if k>size(data,2)
    disp('NO')
elseif mod(size(data,2),k)~=0
    disp('NO')
else
    [N,K] = size(data); %had to reverse the matrix
    K1 = 1:K;
    r = K1(rem(K,K1)==0);
    r = r(r~=1);
    if isempty(find(r==k))
        disp('NO')
        r
    else
        [data1,teacher1] = partition2(data,k);
        %data1 is a structure containing the k partitions
        %teacher1 is a structure containing the teacher for each of the
        %partitions
        a = crossVal(data1,teacher1,hidden);
        
    
    end
    
    
    
    
    
    
end
end


function [d1] = partition(d,k)
%partitions d into k parts
[s1,s2] = size(d);
for i = 1:k
    d1{i} = d(1+(i-1)*s1/k:i*s1/k,:);
end
end

function [d1,f1] = partition2(d,k)
%like the above function but does RANDOM partitions based on the assumption
%that the data is initially bifurcated into 1s and 0s at the middle
[s1,s2] = size(d);
%s1 is rows, s2 is cols
initial_teacher = [ones(1,s2/2) zeros(1,s2/2)];

% 

%seen = [];

for i = 1:k
    temp = [1:s2];
    final_teacher = [ones(1,s2/k)];
    final_teacher = final_teacher.*255;
    dd = zeros(s1,s2/k);
    
    for j = 1:s2/k
        if length(temp)~=1       
            t = randsample(temp,1);
        else
            t = temp(1);
        end
         dd(:,j)= d(:,t);
        temp = temp(temp~=t);
        final_teacher(j) = initial_teacher(t);
        d1{i} = dd;
    end
    f1{i} = final_teacher;
end
end

function [a] = crossVal(data,teacher,hidden)
% this function will perform crossvalidation on all the partitions
totaldata = [data{1:length(data)}];
totalteacher = [teacher{1:length(teacher)}];
sigmoid = @(x) 1 ./ (1 + exp(-x));
z = zeros(1,length(teacher));
z = z*255; %for testing purposes
count = 0;
w0_  = {};
for i = 1:length(data)
    coolList = [1:length(data)];
    c=coolList;
    %i decides which is the index of the validation dataset
    validationData = data{i};
    coolList = coolList(coolList~=i);
    trainData = [data{coolList}];
    trainTeacher = [teacher{coolList}];
    [zz,w,w0,v,v0] = partB(trainData,hidden,trainTeacher);
    validationTeacher = [teacher{setdiff(c,coolList)}];
    % now we have finished training the network
    
    yyy = sigmoid(w' * validationData + w0 * ones(1, size(validationData,2)));
    zzz = sigmoid(v' * yyy + v0);
    
    result{i} = zzz;
    for i = 1:length(zzz)
        
        if zzz(i)>0.5
            zzz(i)=1;
        else
            zzz(i)=0;
        end
    end
    if isequal(validationTeacher,zzz)
        count = count+1;
        w0_{count} = w0;
        w_{count} = w;
        v_{count} = v;
        v0_{count} = v0;
    end
    accuracy = zeros(1,length(validationTeacher));
    for l = 1:length(validationTeacher)
        if validationTeacher(l)==zzz(l)
            accuracy(l) = accuracy(l) + 1;
        end
    end
    accuracy(l) = accuracy(l)/length(validationTeacher);
    
    
end
ww = 0;    
vv = 0;
ww0 = 0;
vv0 = 0;
for i = 1:count
    %z = z+result{i};  %sum of all the zs
    
    ww = ww + w_{count};
    vv = vv+ v_{count};
    ww0 = ww0 + w0_{count};
    vv0 = vv0 + v0_{count};
end
% z = z/length(result); %averaging
ww = ww/count;
vv = vv/count;
ww0 = ww0/count;
vv0 = vv0/count;
% for i = 1:length(z)
%     if z>0.5
%         z =1;
%     else
%         z =0;
%     end
% end
a=0;
for i = 1:length(accuracy)
    a = a + accuracy(i);
end
a = a/length(accuracy);
    
end
    

    