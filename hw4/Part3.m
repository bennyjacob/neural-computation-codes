
% Part 3

clear all

load('hippocampus_data.mat')
tuning_matrix = createTuningMatrix(spike_matrix, pos);

tau = [0.11, 0.21, 0.31, 0.41, 0.51, 0.81, 1.81, 2.81, 5.81, 10.81];
%tau = [0.02, 0.04, 0.06, 0.08, 0.1, 0.11, 0.12];
% for both behavior_time sets
for bt_index = 1:3
    
    % and for multiple values of tau
    for tau_index = 1:length(tau)
        
        decoded_locations = bayesDecode(spike_matrix, tuning_matrix, replay_time(bt_index,:), tau(tau_index));
        
        % subtract 5 from decoded_locations to reset them to original pos range
        decoded_locations = decoded_locations - 5;
        
        range = replay_time(bt_index,1):replay_time(bt_index,2);
        diff = pos(range,:) - decoded_locations;
        sqrerr = sum(diff.^2, 2);
        error = mean(sqrt(sqrerr));
        
        fprintf('For replay_time set #%d and tau = %.2f, the mean error is %.2f\n', bt_index, tau(tau_index), error)
%         if tau(tau_index) == 2.81
%         figure,
%         plotTrajectory(pos,replay_time(bt_index,:),decoded_locations)
%         xlabel(sprintf('Replay time set %d; Mean error = %.2f; tau = %.2f', bt_index, error, tau(tau_index)))
%         end
%         
%         if tau_index == 1 || tau_index == 4 || tau_index == 7
%             figure
%             plot(pos(:,1), pos(:,2), 'Color', [.5 .5 .5])
%             hold on
%             plot(pos(range,1), pos(range,2), 'b', 'LineWidth', 3)
%             plot(decoded_locations(:,1), decoded_locations(:,2), 'rx', 'MarkerSize', 7, 'LineWidth', 3)
%             xlabel(sprintf('Behavior time set %d; Mean error = %.2f; tau = %.2f', bt_index, error, tau(tau_index)))
%         end
        
    end
    
    fprintf('\n')
    
end