function [p] = getP(pos)
gridloc = ceil(pos) + 5;


mini = min(gridloc(:));
maxi = max(gridloc(:));
l= mini:maxi;
p = cell(maxi,maxi);
p(:) = {0};
for i = 1:size(gridloc,1)
    try
        
        p{gridloc(i,1),gridloc(i,2)} = p{gridloc(i,1),gridloc(i,2)}+1;
    catch
        i
    end
end
totsum = cellfun(@sum,p);
totsum = sum(totsum(:));
for i = 1:length(p)
for j = 1:length ([p{i,:}])
p{i,j} = p{i,j}/totsum;
end
end
end