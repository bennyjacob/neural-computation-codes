function [] = getdecoding(spike_matrix,pos,threshold,tuning_matrix)
N = 69;
B=30;
gridloc = ceil(pos) + 5;
%for every position in gridloc, find out how many neurons spiked above a
%certain threshold

%store the set of neurons in a structure

%eg nn(1,5) = [1 2 3 6 60]
%during decoding, if the set of highly firing neurons is [1 2 3 6 60], then
%the decoded position is likely to be 1,5. remember to subtract 5.
tuning_matrices = zeros(69,30,30);
for i = 1:N %this creates individual tuning matrices specific to neurons
    tuning_matrices(i, :, :) = reshape(tuning_matrix(i, :), B, B);
end
nset = cell(30,30);
nset(:) = {[]};
for i = 1:size(gridloc,1)
    seen = [];
    for n = 1:N
        if (tuning_matrices(n,gridloc(i,1),gridloc(i,2)) > threshold) && ~ismember(n,seen)
            seen(end+1) = n;
            z = nset{gridloc(i,1),gridloc(i,2)};
            z(end+1) = n;
            z = sort(z);
            nset{gridloc(i,1),gridloc(i,2)} = z;
        else
            continue
        end
    end
            
end
%now we have nset

