function [decoded_locations] = bayesDecode(spike_matrix, tuning_matrix, behavior_time_indiv, tau)

% tau is in seconds, so multiply by 100 to get # of 10 ms timesteps, then
% divide by 2 to get radius around actual timestep
window = floor(100*tau/2);
decoded_locations = zeros(behavior_time_indiv(2)-behavior_time_indiv(1)+1, 2);
load('hippocampus_data.mat','pos');
[N, P] = size(tuning_matrix);
B = sqrt(P);
tuning_matrices = zeros(N, B, B);
prob = getP(pos); %stores P(x)
for i = 1:N %this creates individual tuning matrices specific to neurons
    tuning_matrices(i, :, :) = reshape(tuning_matrix(i, :), B, B);
end
subset = 1:N;
percent = 25;  % here is where we set the subset percentage of neurons
subset = getSubset(spike_matrix,percent);
for t = behavior_time_indiv(1):behavior_time_indiv(2)
    
    % n is vector containing sum of spikes across tau-specified window for
    % each neuron
    n = sum(spike_matrix(:,t-window:t+window), 2);
    
    % Calculate P(x|n) for all (x, y) : likelihood of location given the
    % spike count vector
    px_given_n = zeros(B, B);
    for y = 1:B
        for x = 1:B
            
            term1 = 0;
            for i = 1:N   %when you want to use the subset, replace by i = subset
                if ~isnan(tuning_matrices(i,x,y))% && tuning_matrices(i,x,y)~=0.0001
                    term1 = term1 + log(tuning_matrices(i,x,y)^n(i));
                else
                    
                    term1 = term1 + 0;
                end
            end
            term2 = 0;
            for i = 1:N  %when you want to use the subset, replace by i = subset
                if ~isnan(tuning_matrices(i,x,y))% && tuning_matrices(i,x,y)~=0.0001
                    term2 = term2 + tuning_matrices(i,x,y);
                else
                    term2 = term2 + 0;
                end
            end
            % posterior distribution of p(x|n)
            %px_given_n(y,x) = prob{x,y}*exp(term1)*exp(-tau*term2);  %use
            %this line instead of the below line when u want to incorporate
            %p(x). comment out the below line when you use the above.
            px_given_n(y,x) = exp(term1)*exp(-tau*term2);
            if isnan(px_given_n(y,x))
                px_given_n(y,x) = 0.0000001;
            
            end
        end
    end
    %for part 4 : 
    
    listofuniques = unique(px_given_n(:));
    maximum = max(listofuniques);
    %uncomment out the below statements in order to use part4's
    %implementation. Then just run part2.m or part3.m and it will do the
    %rest.
%     secondmaximum = max(listofuniques(1:end-1));
%     total = maximum + secondmaximum;
%     maximump = maximum/total; %probability of choosing maximum
%     secondmaximump = secondmaximum/total; %probability of choosing 2nd maximum
%     r = rand(1);
%     if r<=maximump
%         maximum = maximum; %no change. we choose the maximum
%     else
%         maximum = secondmaximum; %we decide to choose the 2nd highest probability
%     end
    [row,column] = find(px_given_n==maximum);
    yindex = row(1);
    xindex = column(1);
    
    % find xindex, and yindex at the location where px_given_n is maximum
    
    decoded_locations(t-behavior_time_indiv(1)+1, :) = [yindex, xindex];
    
end

end
function [subset] = getSubset(origdata,percent)
nold = size(origdata,1);
nnew = floor(nold*percent/100);
oldset = 1:nold;
seen = [];
stop = 0;
count = 0;
subset = [];
while count~=nnew
    blah = randi( nold,1,1);
    if ismember(blah,seen)
        continue
    else
        seen(end+1) = blah;
        subset(end+1) = blah;
        count = count +1;
    end
end
end