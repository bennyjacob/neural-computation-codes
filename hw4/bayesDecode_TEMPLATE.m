function [decoded_locations] = bayesDecode_TEMPLATE(spike_matrix, tuning_matrix, behavior_time_indiv, tau)

% tau is in seconds, so multiply by 100 to get # of 10 ms timesteps, then
% divide by 2 to get radius around actual timestep
window = floor(100*tau/2);
decoded_locations = zeros(behavior_time_indiv(2)-behavior_time_indiv(1)+1, 2);

[N, P] = size(tuning_matrix);
B = sqrt(P);
tuning_matrices = zeros(N, B, B);
for i = 1:N %this creates individual tuning matrices specific to neurons
    tuning_matrices(i, :, :) = reshape(tuning_matrix(i, :), B, B);
end

for t = behavior_time_indiv(1):behavior_time_indiv(2)
    
    % n is vector containing sum of spikes across tau-specified window for
    % each neuron
    n = sum(spike_matrix(:,t-window:t+window), 2);
    
    % Calculate P(x|n) for all (x, y) : likelihood of location given the
    % spike count vector
    px_given_n = zeros(B, B);
    for y = 1:B
        for x = 1:B
            
            term1 = 0;
            for i = 1:N
                if ~isnan(tuning_matrices(i,x,y))% && tuning_matrices(i,x,y)~=0.0001
                    term1 = term1 + log(tuning_matrices(i,x,y)^n(i));
                else
                    term1 = term1 + 0;
                end
            end
            term2 = 0;
            for i = 1:N
                if ~isnan(tuning_matrices(i,x,y))% && tuning_matrices(i,x,y)~=0.0001
                    term2 = term2 + tuning_matrices(i,x,y);
                else
                    term2 = term2 + 0;
                end
            end
            % posterior distribution of p(x|n)
            px_given_n(y,x) = exp(term1)*exp(-tau*term2);
            if isnan(px_given_n(y,x))
                px_given_n(y,x) = 0.0000001;
            
            end
        end
    end
    
    [row,column] = find(px_given_n==max(px_given_n(:)));
    yindex = row(1);
    xindex = column(1);
    
    % find xindex, and yindex at the location where px_given_n is maximum
    
    decoded_locations(t-behavior_time_indiv(1)+1, :) = [xindex, yindex];
    
end