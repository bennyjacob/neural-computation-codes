function[] = q1d()

toff = 100;
Vrest = -70;
R = 100;
C = 100;
tau = 10;
I0 = 0.3;
t = 0:150;
function [v] = V(t)
    for x =t+1;
        
    
        if x>=0 && x<toff
            v(x) = R*I0*(1-exp(-x/tau)) + Vrest;
            
        else
            v(x) = R*I0 * exp( -(x-toff)/tau) + Vrest;
            
        end
    end
end
y=V(t);
plot(t,y)
grid on
end