function [] = q1e()
I_on = 3.0*10^-10;
I_on_time = 0.0;
I_off_time = 0.10;
R = 10*10^6;
C = 1000 * 10^-12;
v_rest = -0.07;
t_start = I_on_time;
t_end = 0.2;
dt = 0.001;
vm = [v_rest];
time = [t_start];
for t = 1:floor((t_end-t_start)/dt)
    step_time = t*dt;
    time = [time step_time];
    if (t-1) * dt >= I_off_time
        I_o = 0;
    else
        I_o = I_on;
    end
    %euler method
    step_vm = vm(end)+dt * ((I_o - ( (vm(end)-v_rest)/R))/C);
    vm = [vm step_vm];
end


%graph
plot(time,vm);
title('Problem 1_e. Membrane potential with injected current (euler method)')
xlabel('Time(seconds)')
ylabel('Membrane potential (volts)')
end

    
        