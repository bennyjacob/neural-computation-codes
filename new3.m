function [zz,bestE,bestV,bestL] = new3(data,hidden,teacher,h_layers,testdata,testteacher,w,w0,v,v0)
%hidden = 4;  % number of hidden units

sigmoid = @(x) 1 ./ (1 + exp(-x));

% initialize data array
% data = [
%         1 1 1 0 1 0 0 1 0;
%         0 0 1 1 1 1 0 0 1;
%         0 1 0 0 1 0 1 1 1;
%         1 0 0 1 1 1 1 0 0;
%         0 1 1 0 1 0 1 1 0;
%         1 0 0 1 1 1 0 0 1;
%         0 1 1 0 1 0 1 1 0;
%         1 0 0 1 1 1 0 0 1;
%         ]';
[N K] = size(data);
mu = 1;
% initialize teacher
%teacher = [ ones(1, K/2) zeros(1, K/2) ];
% half1   = 1 : K/2;
% half2   = K/2 + half1;

% learning rate
eta   = .1;
alpha = .5;  % momentum term

% number of trials
num_trials = 50000;
E      = zeros(1, num_trials);
E(:) = 555555555;
missed = zeros(1, num_trials);
%hidden = num of hidden neurons
% initialize weights

l = struct();
l(1).v = w;
l(1).v0 = w0;
l(1).dv = zeros(N,hidden);
l(1).dv0 = zeros(hidden,1);
bestL = l;

%output layer missing
if h_layers>1
    for i = 2:h_layers
        l(i).v = randn(hidden,hidden);
        l(i).v0 = randn(hidden,1);
        l(i).dv = zeros(hidden,hidden);
        l(i).dv0 = zeros(hidden,1);
        
    end
end
%weights at the output
v = v;
v0 = v0;
dv = zeros(hidden,1);
dv0 = zeros(1);
bestV = {v,v0};
% w  = randn(N,hidden);
% w0 = randn(hidden,1);
% dw = 0; dw0 = 0;

% loop over trials
testerror = zeros(1,num_trials);
trainerror = zeros(1,num_trials);
testerror(:) = 100;
trainerror(:) = 100;
figure,

subplot(121)
hold on
h1(1) = plot([1:100:num_trials],testerror(1:100:end),'b+');

h1(2) = plot([1:100:num_trials],trainerror(1:100:end),'ro');
xlabel('Iterations')
ylabel('Error %')
legend('test','train')
axis([0 num_trials 0 99])


subplot(122)

h2 = plot([1:100:num_trials],E(1:100:end),'go');
axis([0 num_trials 0 110])
xlabel('Iterations')
ylabel('E')
Eprev = 10000;
objective = 1000;
bestE = 1000;   %best energy so far
for t = 1 : num_trials
    iflag = 1;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%% ***********   START OF IMPLEMENTATION %%%%%%%%%%%%%%%%%
    if objective < 1.6e-6 && iflag ==1
        %reset objective
        objective = 1000;
        Eprev = 10000;
        disp('broke')
        %reset 1st hidden layers
        l = struct();
        l(1).v = randn(N,hidden); % these are all w
        l(1).v0 = randn(hidden,1);
        l(1).dv = zeros(N,hidden);
        l(1).dv0 = zeros(hidden,1);
        
        %reset other hidden layers
        if h_layers>1
            for i = 2:h_layers
                l(i).v = randn(hidden,hidden);
                l(i).v0 = randn(hidden,1);
                l(i).dv = zeros(hidden,hidden);
                l(i).dv0 = zeros(hidden,1);
                
            end
        end
        %reset weights at the output
        v = randn(hidden,1);
        v0 = randn(1);
        dv = zeros(hidden,1);
        dv0 = zeros(1);
        
        continue
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%% ***********   END OF IMPLEMENTATION %%%%%%%%%%%%%%%%%
    if t == num_trials
        flag = 1; %breakpoint at last iteration if necessary
    end
    missedv = 0;
    missedt = 0;
    z = cell(h_layers,1);
    
    % compute y layer
    %y = sigmoid(l(1).v' * data + l(1).v0 * ones(1, K));
    z{1} = sigmoid(l(1).v' * data + l(1).v0 * ones(1, K));
    if h_layers>1
        
        for layer = 2:h_layers
            % compute z layer
            z{layer} = sigmoid(l(layer).v' * z{layer-1} + l(layer).v0 * ones(1, K)); %compute outputs of all layers
        end
        
    end
    output = sigmoid(v' * z{h_layers} + v0);
    % compute error
    delta     = teacher - output;
    E(t)      = delta * delta';
    if t>1
        Eprev = E(t-1);
    end
    
    objective = abs(E(t)-Eprev);
    missed(t) = sum( abs( (output > 0.5) - teacher) );
    
    % compute delta_z
    dsig    = output.* (1 - output);
    delta_z = delta .* dsig;
    % accumulate dv
    dv  = eta * z{h_layers} * delta_z' + alpha * dv ;
    dv0 = eta * sum(delta_z) + alpha * dv0;
    v  = v  + dv;
    v0 = v0 + dv0;
    %z{hlayer}
    layer = h_layers;
    dsig    = z{layer} .* (1 - z{layer});
    delta_z = dsig .*( v * delta_z); %backpropogation
    
    % accumulate dv
    if h_layers>1
        %go from the uppermost hidden to the 2nd lowermost
        l(layer).dv  = eta * z{layer-1} * delta_z' + alpha * l(layer).dv ;
        l(layer).dv0 = eta * sum(delta_z,2) + alpha * l(layer).dv0;
        
        %update weights
        l(layer).v  = l(layer).v  + l(layer).dv;
        l(layer).v0 = l(layer).v0 + l(layer).dv0;
        
        %compute deltas for inner hidden layers
        for layer = h_layers-1:-1:2
            % compute delta_y
            dsig    = z{layer} .* (1 - z{layer});
            delta_z = dsig .* (l(layer+1).v * delta_z); %backpropogation
            
            % accumulate dv
            l(layer).dv  = eta * z{layer-1} * delta_z' + alpha * l(layer).dv ;
            l(layer).dv0 = eta * sum(delta_z,2) + alpha * l(layer).dv0;
            
            %update weights
            l(layer).v  = l(layer).v  + l(layer).dv;
            l(layer).v0 = l(layer).v0 + l(layer).dv0;
        end
    end
    %1st hidden layer
    layer = 1;
    dsig    = z{layer} .* (1 - z{layer});
    if h_layers>1
        delta_z = dsig .* (l(layer+1).v * delta_z); %backpropogation
    end
    
    % accumulate dv
    l(layer).dv  = eta * data * delta_z' + alpha * l(layer).dv ;
    l(layer).dv0 = eta * sum(delta_z,2) + alpha * l(layer).dv0;
    
    %update weights
    l(layer).v  = l(layer).v  + l(layer).dv;
    l(layer).v0 = l(layer).v0 + l(layer).dv0;
    %finished updating all the hidden layers
    
    if h_layers==1
        for p = 1:size(testdata,2)
            
            hidden1 = sigmoid(l(1).v' * testdata(:,p) + l(1).v0);
            result = sigmoid(v' * hidden1 + v0);
            if double(result>0.5)~=testteacher(p)
                missedv = missedv+1;
            end
        end
    elseif h_layers>1
        for p = 1:size(testdata,2)
            
            hidden1 = sigmoid(l(1).v' * testdata(:,p) + l(1).v0);
            for j = 2:h_layers
                hidden1 = sigmoid(l(j).v' * hidden1 + l(j).v0); %compute the next hidden layer based on the previous hidden layer
            end
            result = sigmoid(v' * hidden1 + v0); %compute the result based on the last hidden layer
            if double(result>0.5)~=testteacher(p)
                missedv = missedv+1;
            end
        end
    end
    set(h1(1),'Ydata',testerror(1:100:end));
    set(h1(2),'Ydata',trainerror(1:100:end));
    set(h2,'Ydata',E(1:100:end));
    drawnow
    
    zz = double(output>0.5);
    for i = 1:length(zz)
        if zz(i)~=teacher(i)
            missedt = missedt+1;
        end
    end
    trainerror(t) = missedt/K * 100;
    testerror(t) = missedv/size(testdata,2) * 100;
    if E(t) < bestE
        bestE = E(t);
        bestV = {v,v0};
        bestL = l;
    end
    
end
hold off
% figure,
% plot([1:100:num_trials],testerror(1:100:end),'b+',[1:100:num_trials],trainerror(1:100:end),'ro')
% legend('test','train')



end