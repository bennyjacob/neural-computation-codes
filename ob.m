function o = ob(sam)





load('matlab.mat','hidden');
load('matlab.mat','data');
load('matlab.mat','teacher');
w = zeros(2,hidden);
w0 = zeros(hidden,1);
v = zeros(hidden,1);
v0 = zeros(1,1);
w(1,:) = sam(1,1:hidden);
w(2,:) = sam(1,hidden+1:2*hidden);
w0 = sam(1,2*hidden+1:3*hidden);
w0 = w0';
v = sam(1,3*hidden+1:4*hidden);
v = v';
v0 = sam(1,4*hidden+1);
v0 = v0';

%data = getdata();
[N,K] = size(data);
%teacher = getteacher();
sigmoid = @(x) 1 ./ (1 + exp(-x));
hiddenoutput = sigmoid(w' * data + w0 * ones(1, K));
finaloutput = sigmoid(v'*hiddenoutput + v0);
delta     = teacher - finaloutput;
E = delta * delta' ;
o = E;
missed = sum( abs( (finaloutput > 0.5) - teacher) );
end

