% patient#    asymptomatic or symptomatic
%=========================================
% 01          symptomatic
% 02          asymptomatic
% 03          asymptomatic
% 04          asymptomatic
% 05          symptomatic
% 06          symptomatic
% 07          symptomatic
% 08          symptomatic
% 09          asymptomatic
% 10          symptomatic
% 11          asymptomatic
% 12          symptomatic
% 13          symptomatic
% 14          asymptomatic
% 15          symptomatic
% 16          asymptomatic
% 17          asymptomatic

patientsClassLabel = {'symptomatic';'asymptomatic';'asymptomatic';'asymptomatic';'symptomatic';'symptomatic';'symptomatic';'symptomatic';'asymptomatic';'symptomatic';'asymptomatic';'symptomatic';'symptomatic';'asymptomatic';'symptomatic';'asymptomatic';'asymptomatic'}

% remove patient# 8, 13, 17 due to data inconsistency
patientsClassLabel = patientsClassLabel([1:7, 9:12, 14:16],:);
