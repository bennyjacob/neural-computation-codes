% Not sure how to loop through csv files to save 3 var, so I typed it all out
%===============================================================================
% Use patient1 to patient12 for training data; 13 to 17 for testing data

% Read tab-delimited file
%%testing loop
%for i = 1:3
%    %['data'num2str(i), 'timeLabel'num2str(i), 'geneLabel'num2str(i)]
%    %eval(['['data'num2str(i), 'timeLabel'num2str(i), 'geneLabel'num2str(i)]' = tblread(['flu',filesep,'patient',i,'.csv'], '\t');
%    ['data',i] = tblread(['flu',filesep,'patient',i,'.csv'], '\t');
%    %eval(['['A_' num2str(i),'B_'num2str(i)]''=3,4;']);
%    %eval(['data'num2str(i), 'timeLabel'num2str(i), 'geneLabel'num2str(i)] '=' 1,2,3;]);
%    %figure('Name',geneLabel(1,:)), plot(data(1,:))
%end

% Original data information:
% 17 patients, 11961 gene expressions
% Filtered data information:
% 14 patients, 11961 gene expressions

% Drop data that is in consistent
[dataPatient01, timeLabel01, geneLabel01] = tblread(['flu',filesep,'patient1','.csv'], '\t');
[dataPatient02, timeLabel02, geneLabel02] = tblread(['flu',filesep,'patient2','.csv'], '\t');
[dataPatient03, timeLabel03, geneLabel03] = tblread(['flu',filesep,'patient3','.csv'], '\t');
[dataPatient04, timeLabel04, geneLabel04] = tblread(['flu',filesep,'patient4','.csv'], '\t');
[dataPatient05, timeLabel05, geneLabel05] = tblread(['flu',filesep,'patient5','.csv'], '\t');
[dataPatient06, timeLabel06, geneLabel06] = tblread(['flu',filesep,'patient6','.csv'], '\t');
[dataPatient07, timeLabel07, geneLabel07] = tblread(['flu',filesep,'patient7','.csv'], '\t');
%[dataPatient08, timeLabel08, geneLabel08] = tblread(['flu',filesep,'patient8','.csv'], '\t');
[dataPatient09, timeLabel09, geneLabel09] = tblread(['flu',filesep,'patient9','.csv'], '\t');
[dataPatient10, timeLabel10, geneLabel10] = tblread(['flu',filesep,'patient10','.csv'], '\t');
[dataPatient11, timeLabel11, geneLabel11] = tblread(['flu',filesep,'patient11','.csv'], '\t');
[dataPatient12, timeLabel12, geneLabel12] = tblread(['flu',filesep,'patient12','.csv'], '\t');
%[dataPatient13, timeLabel13, geneLabel13] = tblread(['flu',filesep,'patient13','.csv'], '\t');
[dataPatient14, timeLabel14, geneLabel14] = tblread(['flu',filesep,'patient14','.csv'], '\t');
[dataPatient15, timeLabel15, geneLabel15] = tblread(['flu',filesep,'patient15','.csv'], '\t');
[dataPatient16, timeLabel16, geneLabel16] = tblread(['flu',filesep,'patient16','.csv'], '\t');
%[dataPatient17, timeLabel17, geneLabel17] = tblread(['flu',filesep,'patient17','.csv'], '\t');
