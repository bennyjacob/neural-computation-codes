function [ recon ] = rbm_reconstruct( rbm,original)
%RBM_RECONSTRUCT reconstructs an image
%
% INPUTS:
%   original ...........: a (# examples  by # visible units) matrix. 
%                           
% OUTPUTS:
%   recon ..............: a (# examples  by # visible units) matrix.
%                           Computed by sampling the hidden layer given the 
%                           visisble units in 'original' and then sampling
%                           the visible layer given the sampled hidden
%                           layer

% function [ rbm ] = initialize_rbm(n_visible, n_hidden, batch_size, sparsity,...
%                                         sparsity_lambda)
%safdfaasd = asd * fafsfsa
%rbm = initialize_rbm(size(original,2),size(original,2),size(original,1));
H_sample = sample(rbm_ph_given_v(rbm, original));
V_sample = sample(rbm_pv_given_h(rbm,H_sample));
recon = V_sample;
end

