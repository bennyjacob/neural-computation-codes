
data = zeros(10,784);
for i = 1:10
    im = imread(['IMAGES/im.' num2str(i) '.tif']);
    im = imresize(im,[28,28]);
    bin = double(im>100);
    bin = reshape(bin,784,1);
    data(i,:) = bin;
end
w = learn_hopfield_net(data(1:8,:));
hop_settle_and_show(w, data(1:8,:), 2.^[5:2:13], h1 );
