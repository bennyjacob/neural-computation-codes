load('data.mat')
h1 = figure(1);
w = learn_hopfield_net(data_10(1,:));
hop_settle_and_show(w, data_10(1,:), 2.^[5:2:13], h1 )
w = learn_hopfield_net(data_10(1:2,:));
hop_settle_and_show(w, data_10(1:2,:), 2.^[5:2:13], h1 )
w = learn_hopfield_net(data_10(1:3,:));
hop_settle_and_show(w, data_10(1:3,:), 2.^[5:2:13], h1 );
w = learn_hopfield_net(data_10(1:4,:));
hop_settle_and_show(w, data_10(1:4,:), 2.^[5:2:13], h1 );
w = learn_hopfield_net(data_10(1:5,:));
hop_settle_and_show(w, data_10(1:5,:), 2.^[5:2:13], h1 );
w = learn_hopfield_net(data_10(1:6,:));
hop_settle_and_show(w, data_10(1:6,:), 2.^[5:2:13], h1 );
w = learn_hopfield_net(data_10(1:5,:));
hop_settle_and_show(w, data_10(1:5,:), 2.^[5:2:13], h1 );
w = learn_hopfield_net(data_10(1:7,:));
hop_settle_and_show(w, data_10(1:7,:), 2.^[5:2:13], h1 );
w = learn_hopfield_net(data_10(1:8,:));
hop_settle_and_show(w, data_10(1:8,:), 2.^[5:2:13], h1 );
w = learn_hopfield_net(data_10(1:9,:));
hop_settle_and_show(w, data_10(1:9,:), 2.^[5:2:13], h1 );
w = learn_hopfield_net(data_10(1:10,:));
hop_settle_and_show(w, data_10(1:10,:), 2.^[5:2:13], h1 );