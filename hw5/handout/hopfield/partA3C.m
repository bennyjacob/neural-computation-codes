load('data.mat')
origdata = data_10(1:2,:);
noise = .25;
numpixels = floor(noise*length(origdata));
count = 0;
while count<numpixels
    
    i = randi(size(origdata,1));
    j = randi(size(origdata,2));
    
    %    25% probability of choosing this pixel to change
    if origdata(i,j) == 1
        origdata(i,j) = 0;
    else
        origdata(i,j) = 1;
    end
    count = count + 1;
    
end

w = learn_hopfield_net(data_10(1:2,:));
hop_settle_and_show(w, origdata(1:2,:), 2.^[5:2:13], h1 );

