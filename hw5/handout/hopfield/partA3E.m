data = zeros(10,784);
for i = 1:8
    im = imread(['IMAGES/im.' num2str(i) '.tif']);
    im = imresize(im,[28,28]);
    bin = double(im>100);
    bin = reshape(bin,784,1);
    data(i,:) = bin;
end

w = learn_hopfield_net(data(1:8,:)); %learn network using original images
figure,
hop_settle_and_show(w, data(1:8,:), 2.^[5:2:13], h1 );
data = zeros(8,784);
for i = 1:8
    im = imread(['IMAGES/im.' num2str(i) '.tif']);
    im = imresize(im,[28,28]);
    im = double(im);
    mask = zeros(28,28);
    mask(1:14, 1:14) = 1;
    inverted_mask = ones(28,28);
    inverted_mask(1:14, 1:14) = 0;
    im = im.*mask;
    %im = im + inverted_mask.*double(rand(size(im))>.5);
    bin = double(im>100);
    bin = bin + inverted_mask.*double(rand(size(bin))>.5);
    bin = reshape(bin,784,1);
    data(i,:) = bin;
end
figure,
hop_settle_and_show(w, data(1:8,:), 2.^[5:2:13], h1 ); %settle the partial images