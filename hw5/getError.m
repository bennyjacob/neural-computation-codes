function [error] = getError(img1,img2)
error = 0;
img1 = rgb2gray(img1);
img2 = rgb2gray(img2);
for i = 1: size(img1,1)
for j = 1:size(img1,2)
    
    error = error + (double(img1(i,j)>100)-double(img2(i,j)>100))^2;
    
end
end
end
