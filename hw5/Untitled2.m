rbm = initialize_rbm(784,500,100);
rbm_200_1 = train_rbm(rbm, data_1000, 200);
weights1 = rbm_200_1.w;
H_layer1 = sample(rbm_ph_given_v(rbm_200_1, data_1000)); %1000x500
rbm = initialize_rbm(500,200,100);
rbm_200_2 = train_rbm(rbm, H_layer1, 200); % weights have been trained
hsample1 = sample(rbm_ph_given_v(rbm_200_1, data_100)); %sample hidden layer from visible layer data
hsample2 = sample(rbm_ph_given_v(rbm_200_2, hsample1)); %sample 2nd hidden layer using 1st hidden as visible layer
vsample1 = sample(rbm_pv_given_h(rbm_200_2,hsample2)); %sample 1st hidden layer using 2nd as hidden
vsample2 = sample(rbm_pv_given_h(rbm_200_1,vsample1)); %sample 1st visible layer using 1st hidden layer as hidden
for i = 1:100
subaxis(10,10,i, 'Spacing', .0, 'Padding', 0, 'Margin', 0);
imshow(reshape(vsample2(i,:),28,28),[]);
axis off
end