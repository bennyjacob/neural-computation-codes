For stacking up the rbms, the general procedure is given in "Untitled2.m"

Set the parameters as needed and don't forget to load the 'data.mat'.


All code for part A is in the folder 'handout/hopfield'.

All code for part B is in the folder 'handout/rbm'.


The array "IMAGES.mat" (for part B2C) has been deleted from this compressed archive because it is too fat and takes up a lot of space. So if you want to test part B2C, 
you will need to download that data file from blackboard again.

Add this entire Hw5 folder and all subdirectories to the matlab path so that you can easily load the .mat files needed for the different questions (they are currently in different
subfolders).