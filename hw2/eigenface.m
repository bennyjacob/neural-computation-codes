load('facedatabase.mat')
colormap gray
for i = 1:48
imagesc(reshape(faces2(:,i),60,64)),
axis image
drawnow
pause(0.25)
end
s=0;
for i = 1:size(faces2,2)
s = s+faces2(:,i);
end
meanface = s/48;

faces2_1 = faces2;
for i =1: size(faces2,2)
faces2_1(:,i) = faces2(:,i)-meanface;
end
figure
colormap('gray');
 c = cov(faces2_1',1);
[U,S,V] = svd(faces2_1);
for idx = 1:2
    subplot(1,2,idx);
    z = U(:,idx);
    %contrast renormalization
    z = z - min(z);
    z = z / max(z);
    imagesc(reshape(z,60,64));
    set(gca,'xticklabel',[]);
    set(gca,'yticklabel',[]);
    title(sprintf('%0.5g',S(idx,idx)));

    %set(gca, 'visible', 'off'); 
end