delta = clf;
clear;


num_patches = 10000;
num_images = 10;
limit = 2e-06; 
num_components = 64;
n = 2*10^-10;

samples_per_image = num_patches/num_images;

%get training dataset
data = [];
data= [data extract_patches(imread('im.1.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.2.tif'),8,samples_per_image )];
data= [data extract_patches(imread('im.3.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.4.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.5.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.6.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.7.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.8.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.9.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.10.tif'),8,samples_per_image)];


% for i = 1:numel(data)
% mean = mean + data(i);
% end
% mean = mean/numel(data);
%0 mean the data
%verify that we take mean of all patches and subtract and not
%individual patches and subtract

[xdim, ydim] =size(data)


data_mean = mean(data')';
scattermean = data_mean*ones(1,10000);
data = data - scattermean;


[xdim ydim] = size(data);

%hebbian learning using sanger's rule
%initializing weights to random variables
w = randn(xdim, num_components);

%iterate until delta is smaller then limit
delta = 1;
delta = 100000;
numadjust = 0;  %????
iteration = 0;   %?????!!!?

% slow version, ok for learning a few components  
% you may want to vectorize the codes for learning all 64 components
% you can also use the while loop: while abs(delta) > limit
% here, we will just learn three neurons (j), printing out the weights every 30 iterations 
% of the data set
delta2 = 51; %meaningless initial value to get through the first loop
while delta2>0.1  % here limit is 0.1
% for outerloop = 1:20
%   for innerloop = 1:30
      %disp(innerloop)
    for idy = 1:ydim
        xcur = data(:,idy);
        ycur = w'*xcur;
        w_delta = zeros(xdim, 64);
        for j = 1:64
            
                
            for i = 1:64
                
                % compute sum w_ik y_k
                sumwy = 0;
                for k=1:j
                    sumwy = sumwy +w(i,k)*ycur(k);
                end
                
                
                
                               
                w_delta(i,j) = n*(ycur(j)* xcur(i)  - ycur(j)*sumwy);%%  FILL IN LEARNING RULE 
            end
        end
        w = w + w_delta;
    sum(sum(w));
    end
  
  
   
    
    delta2 = abs(delta - sum(sum(w)))  %previous delta minus current delta
    delta = sum(sum(w))

end

 %display results (all)
    figure
    colormap('gray');

    for idx = 1:10
        subplot(1,10, idx);
        z = w(:,idx);
        %contrast renormalization
        z = z - min(z);
        z = z / max(z);
        imagesc(reshape(z,8,8));
        set(gca, 'visible', 'off'); 
%title(sprintf('%0.5g',w(idx,idx)));
    end


im = imread('im11.tif');
[x y] = size(im);
results = zeros(x,y);
step = 8;
numeig = 10;
coeffs = zeros(10,3600);
count = 0;
for idx = 0:(x/step)-1
    for idy = 0:(y/step)-1
        count = count+1;
       
        patch = im(((idx*8)+1):((idx+1)*8),((idy*8)+1):((idy+1)*8)); %chooses an 8x8 patch?
        
        patch =double( reshape(patch, 1,64));
        
        n_patch = zeros(64,1);

        %dot product and recreate patch
        %n_patch = dot(patch(
        for idcoeff = 1:10
             n_patch = n_patch + w(:,idcoeff)*dot(patch,w(:,idcoeff)); %%   TASK: synthesized a new patch based on 10 PCs 
             coeffs(idcoeff,count) = dot(patch,w(:,idcoeff));
        end  
        
        %write back results
        n_patch = reshape(n_patch,8,8);
        results(((idx*8)+1):((idx+1)*8),((idy*8)+1):((idy+1)*8)) = n_patch;
        
    end
end

figure
colormap('gray');
%contrast normalization
results = results - min(min(results));
results = results / max(max(results));
imshow(results);
title('Recreated with PCA (44)');

