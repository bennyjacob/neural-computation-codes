delta = clf;
clear;


num_patches = 10000;
num_images = 10;
limit = 2e-06; 
num_components = 3;
n = 2*10^-10;

samples_per_image = num_patches/num_images;

%get training dataset
data = [];
data= [data extract_patches(imread('im.1.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.2.tif'),8,samples_per_image )];
data= [data extract_patches(imread('im.3.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.4.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.5.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.6.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.7.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.8.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.9.tif'),8,samples_per_image)];
data= [data extract_patches(imread('im.10.tif'),8,samples_per_image)];

%0 mean the data
%verify that we take mean of all patches and subtract and not
%individual patches and subtract

[xdim, ydim] =size(data)


data_mean = mean(data')';
scattermean = data_mean*ones(1,10000);
data = data - scattermean;


[xdim ydim] = size(data);

%hebbian learning using sanger's rule
w = randn(xdim, num_components);

%iterate until delta is smaller then limit
delta = 1;
numadjust = 0;
iteration = 0;

% slow version, ok for learning a few components  
% you may want to vectorize the codes for learning all 64 components
% you can also use the while loop: while abs(delta) > limit
% here, we will just learn three neurons (j), printing out the weights every 30 iterations 
% of the data set

for outerloop = 1:20
  for innerloop = 1:30
    for idy = 1:ydim
        xcur = data(:,idy);
        ycur = w'*xcur;
        w_delta = zeros(xdim, 3);
        for j = 1:3
            for i = 1:64
                               % compute sum w_ik y_k
                sumwy = 0;
                for k=1:j
                    sumwy = sumwy +w(i,k)*ycur(k);
                end               
                w_delta(i,j) = n*(ycur(j)* xcur(i)  - ycur(j)*sumwy);%%  FILL IN LEARNING RULE 
            end
        end
        w = w + w_delta;
    sum(sum(w));
    end
    ycur
  end
  ycur
  
    %display results (all)
    figure
    colormap('gray');

    for idx = 1:3
        subplot(1,3, idx);
        z = w(:,idx);
        %contrast renormalization
        z = z - min(z);
        z = z / max(z);
        imagesc(reshape(z,8,8));
        set(gca, 'visible', 'off'); 
    end
    delta = sum(sum(w))

end




